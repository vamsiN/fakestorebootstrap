import { RiErrorWarningLine } from 'react-icons/ri'


export default function Error() {

    return (
        <>
            <div >
                <div className='container text-center mt-5 '>
                    < RiErrorWarningLine />
                    <p className='text-danger'>An Error occured</p>
                </div>

            </div>
        </>
    )

}