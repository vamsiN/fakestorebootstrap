import './App.css';
import ProductCard from './Components/ProductCard';
import { Component } from 'react'
import axios from 'axios'
import './Components/Loader'
import Loader from './Components/Loader';
import Error from './Components/Error.js';



class App extends Component {


  state = {
    productsList: [],
    status: 'loading',
    
    
  }

  componentDidMount = () => {

    axios.get('https://fakestoreapi.com/products')
      .then((response) => {
        console.log(response)
        this.setState({
          productsList: response.data,
          status: 'loaded'          
        })
      })
      .catch((error) => {
        console.log(error)
        this.setState({
          status: 'error',
          
        })
      })

  }


  render() {
    const { productsList, status } = this.state
    console.log(productsList)
    return (
      <>

        {status === 'loading' ?
          <Loader />
          :
          undefined
        }

        {status === 'error' ?
          < Error />
          :
          undefined
        }

        {status === 'loaded' ?

          <>
            <div>
              <div className='container-fluid bg-dark'> 
                <div className='row'>
                  <div className='col-12 '>
                    <h1 className='text-center text-danger m-5'>Our Products</h1>
                  </div>
                </div>
                <div className='row justify-content-spacearound'>
                  {productsList.map((eachProduct) => (
                    < ProductCard eachProductDetails={eachProduct} key={eachProduct.id} />
                  ))}

                </div>
              </div>

            </div>
          </>


          :
          undefined
        }







      </>
    );
  }

}

export default App;
