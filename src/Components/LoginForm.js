import logo from './logo.svg';
import './App.css';


import { Component } from 'react'

class App extends LoginForm {

  state = {
    firstName: '',
    lastName: '',
    email: '',
  }

  handleInputChange = (event) => {

    console.log(event.target.id)

    this.setState({
      [event.target.name]: event.target.value
    })


    // if(event.target.id == 'firstName'){
    //   this.setState({
    //     firstName:event.target.value
    //   })
    // }

    // if(event.target.id == 'lastName'){
    //   this.setState({
    //     lastName:event.target.value
    //   })
    // }

    // if(event.target.id == 'email'){
    //   this.setState({
    //     email:event.target.value
    //   })
    // }

  }


  render() {
    return (
      <div className='container'>
        <form className='form-'>
          <label htmlFor='firstName' className='form-label'>Firstname</label>
          <input id='firstName' name='firstName' className='form-control' onChange={this.handleInputChange} />

          <label htmlFor='lastName' className='form-label'>lastname</label>
          <input id='lastName' className='form-control' onChange={this.handleInputChange} />

          <label htmlFor='email' className='form-label'>email</label>
          <input id='email' className='form-control' onChange={this.handleInputChange} />

          <button className='btn btn-primary'>Submit</button>

        </form>

      </div>
    );
  }

}

export default LoginForm;
