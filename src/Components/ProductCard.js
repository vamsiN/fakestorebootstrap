const ProductCard = (props) => {
    const { eachProductDetails } = props
    // console.log(eachProductDetails)

    return (
        <>


            <div className='col-10 col-sm-6 col-md-5 col-lg-3 mb-4'>
                <div className="card h-100 text-center p-4" >
                    <img src={eachProductDetails.image} className="card-img-top card-fluid" height='320px' alt="product-image" />
                    <div class="card-body">
                        <h6 className="card-title">{eachProductDetails.title.substring(0, 18)}...</h6>
                        <p className="card-text text-danger">Price: $ {eachProductDetails.price}</p>
                        <p className='text-success'>Rating: {eachProductDetails.rating.rate} {`(${eachProductDetails.rating.count})`}</p>
                        <button className="btn btn-outline-primary m-1">Buy Now</button>

                        {/* <button className="btn btn-outline-dark m-1">Whislist</button> */}

                    </div>
                </div>
            </div>



        </>

    )

}

export default ProductCard